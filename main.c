#include <stdlib.h>
#include <string.h>
#include <printf.h>
#include <stdio.h>
#include <unistd.h>
#include "ipc.h"
#include "pa1.h"
#include "main.h"
#include <fcntl.h>
#include "common.h"
#include <time.h>
#include <wait.h>
#include <assert.h>
#include <sys/stat.h>
#include <fcntl.h>

const char *pipe_error = "Pipe error";
const char *file_open_error = "File open error";
const char *invalid_argument_error = "Pipe error";
const char *invalid_message_started_received = "Process %i received invalid STARTED message\n";
const char *invalid_message_done_received = "Process %i received invalid DONE message\n";
const int LOG_BUFFER_LEN = 255;
int action_log_fd; //log_handle

void log_action(int sid, const char *msg) {
    write(action_log_fd, msg, strlen(msg));
    write(STDOUT_FILENO, msg, strlen(msg));
}

int send_msg(t_process *me, MessageType action) {
    assert(action == STARTED || action == DONE);

    char str[LOG_BUFFER_LEN];
    Message *msg = (Message*) malloc(sizeof(Message));

    const char *string = action == STARTED?log_started_fmt:log_done_fmt;

    sprintf(msg->s_payload, string, me->sid, getpid(), getppid());
    MessageHeader mh;
    mh.s_magic = MESSAGE_MAGIC; mh.s_type = action;
    mh.s_local_time = time(NULL);
    mh.s_payload_len = (uint16_t) strlen(msg->s_payload);
    msg->s_header = mh;

    sprintf(str, string, me->sid, getpid(), getppid());
    log_action(me->sid, str);

    int result = send_multicast((void*) me, msg);
    free(msg);

    return result;
}

int receive_action(t_process *me, MessageType action) {
    assert(action == STARTED || action == DONE);

    char str[LOG_BUFFER_LEN];
    Message *msg = (Message*) malloc(sizeof(Message));

    int f = 1;
    for (int i = 0; i < me->procsNum; ++i)
        if (i != PARENT_ID) {
            receive(me, i, msg);
            if (!(f = msg->s_header.s_type == action)) break;
        }

    if (!f) fprintf(stderr, action == STARTED?invalid_message_started_received:invalid_message_done_received, me->sid);

    sprintf(str, action == STARTED?log_received_all_started_fmt:log_received_all_done_fmt, me->sid);
    log_action(me->sid, str);

    free(msg);
    return !f;
}

void
thread_work(const int sid, int **pipes, int procsNum) {
    t_process *me = (t_process*) malloc(sizeof(t_process));
    me->pipes = pipes;
    me->procsNum = procsNum;
    me->sid = sid;

    send_msg(me, STARTED);

    receive_action(me, STARTED);

    send_msg(me, DONE);

    receive_action(me, DONE);
}

int
get_procs_num(const int argc, char* argv[]) {
    int procsNum = 0;
    for (int i = 1; i < argc; ++i) {
        if (strcmp(argv[i], "-p") == 0) {
            if (i + 1 < argc) {
                procsNum = atoi(argv[++i]);
                if (procsNum < 0 || procsNum > 10) {
                    fputs(invalid_argument_error, stderr);
                    return -1;
                }
            } else {
                fputs("-p option requires one argument.", stderr);
                return -1;
            }
        } else {
            fprintf(stderr, "invalid parameter %s", argv[i]);
            return -1;
        }
    }
    return procsNum;
}

int
main(int argc, char* argv[]) {
    if (argc < 3) {
        fprintf(stderr, "Usage: %s -p processes_number", argv[0]);
        return 1;
    }

    char str[LOG_BUFFER_LEN];

    if ((action_log_fd = open(events_log,
                      O_TRUNC | O_WRONLY | O_CREAT,
                      S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IROTH
                     )) < 0) {
        perror(file_open_error);
        return 1;
    }

    // Get da procs num
    int procsNum;
    if ((procsNum = get_procs_num(argc, argv)) < 0) return 1;

    /*
    int inputs[procsNum][procsNum];
    int outputs[procsNum][procsNum];
    puts("hell");

    */
    int ***pipes = (int***) calloc((size_t)procsNum, sizeof(int*));

    int **inputs = (int**) calloc((size_t)procsNum, sizeof(int*)),
            **outputs = (int**) calloc((size_t)procsNum, sizeof(int*));
    for (int i = 0; i < procsNum; ++i) {
        pipes[i] = (int**) calloc((size_t)procsNum, sizeof(int*));
        for (int j = 0; j < procsNum; ++j) {
            pipes[i][j] = (int*) calloc(2, sizeof(int));
        }
        inputs[i] = (int*) calloc((size_t)procsNum, sizeof(int*));
        outputs[i] = (int*) calloc((size_t)procsNum, sizeof(int*));
    }
    int pids[procsNum];
    int p[2];

    for (int i = 0; i < procsNum; ++i) {
        if (pipe(p) != 0) {
            fputs(pipe_error, stderr);
            return 1;
        }
        /*
        outputs[i][PARENT_ID] = p[1];
        inputs[PARENT_ID][i] = p[0];
         */
        pipes[i][PARENT_ID][OUTPUT] = p[1];
        pipes[PARENT_ID][i][INPUT] = p[0];

        if (i != PARENT_ID) {
            if (pipe(p) != 0) {
                fputs(pipe_error, stderr);
                return 1;
            }
            /*
            outputs[PARENT_ID][i] = p[1];
            inputs[i][PARENT_ID] = p[0];
             */
            pipes[PARENT_ID][i][OUTPUT] = p[1];
            pipes[i][PARENT_ID][INPUT] = p[0];
        }
    }

    // Now create forks already
    for (int i = 0; i < procsNum; ++i) {
        if (i != PARENT_ID) {
            fflush(stdout);
            for (int j = 0; j < procsNum; ++j) {
                if (j != PARENT_ID) {
                    if (pipe(p) != 0) {
                        fputs(pipe_error, stderr);
                        return 1;
                    }
                    /*
                    outputs[j][i] = p[1];
                    inputs[i][j] = p[0];
                     */
                    pipes[j][i][OUTPUT] = p[1];
                    pipes[i][j][INPUT] = p[0];
                }
            }
        }
    }

    for (int i = 0; i < procsNum; ++i) {
        if (i != PARENT_ID) {
            if ((pids[i] = fork()) == 0) {
                thread_work(i, pipes[i], procsNum);
                return 0;
            }
        }
    }

    t_process *self = (t_process*) malloc(sizeof(t_process));
    self->sid = PARENT_ID;
    self->procsNum = procsNum;
    self->pipes = pipes[self->sid];

    for (int i = 0; i < procsNum; ++i) {
        if (i != PARENT_ID) {
            Message *msg = (Message *) malloc(sizeof(Message));

            receive(self, i, msg);

            /*
            if (msg->s_header.s_type == STARTED) {
                sprintf(str, log_started_fmt, i);
                log_action(0, str);
            } else fprintf(stderr, invalid_message_started_received, self->sid);
             */
            free(msg);
        }
    }

    sprintf(str, log_received_all_started_fmt, PARENT_ID);
    log_action(PARENT_ID, str);

    for (int i = 0; i < procsNum; ++i) {
        if (i != PARENT_ID) {
            Message *msg = (Message *) malloc(sizeof(Message));
            receive(self, i, msg);

            /*
            if (msg->s_header.s_type == DONE) {
                sprintf(str, log_done_fmt, i);
                log_action(0, str);
            } else fprintf(stderr, invalid_message_done_received, self->sid);
             */
            free(msg);
        }
    }

    sprintf(str, log_received_all_done_fmt, PARENT_ID);
    log_action(PARENT_ID, str);

    for (int i = 0; i < procsNum; ++i)
        waitpid(pids[i], 0, 0);

    return 0;
}
