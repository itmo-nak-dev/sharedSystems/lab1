//
// Created by whobscr on 28.03.17.
//

#ifndef LAB1_MAIN_H
#define LAB1_MAIN_H

#define OUTPUT 0
#define INPUT 1

typedef struct {
    int sid;
    int procsNum;
    int **pipes;
    /*
    int *inputs;
    int *outputs;
     */
} t_process;

#endif //LAB1_MAIN_H
