//
// Created by whobscr on 28.03.17.
//

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include "ipc.h"
#include "main.h"

//------------------------------------------------------------------------------

/** Send a message to the process specified by id.
 *
 * @param self    Any data structure implemented by students to perform I/O
 * @param dst     ID of recepient
 * @param msg     Message to send
 *
 * @return 0 on success, any non-zero value on error
 */

int
send(void * self, local_id dst, const Message * msg) {
    t_process *self_p = (t_process*) self;

    assert(msg != NULL && "Check if sending message is not null");
    assert(dst >= 0 && dst <= 10 && "Check whether destination number is valid");
    assert(msg->s_header.s_magic == MESSAGE_MAGIC && msg->s_header.s_payload_len == strlen(msg->s_payload) && "Check whether message is valid formed");
    assert(self_p->procsNum >= 0 && self_p->procsNum <= 10 && "Check whether processes number is valid");
    assert(self_p->sid >= 0 && self_p->sid <= 10 && "Check whether self id is valid");

    if ((int) write(self_p->pipes[dst][OUTPUT], &msg->s_header, sizeof(MessageHeader)) <= 0) {
        perror("error while sending");
        return -1;
    }
    if ((int) write(self_p->pipes[dst][OUTPUT], msg->s_payload, strlen(msg->s_payload)) <= 0) {
        perror("error while sending");
        return -1;
    }
    return 0;
}

//------------------------------------------------------------------------------

/** Send multicast message.
 *
 * Send msg to all other processes including parrent.
 * Should stop on the first error.
 *
 * @param self    Any data structure implemented by students to perform I/O
 * @param msg     Message to multicast.
 *
 * @return 0 on success, any non-zero value on error
 */
int
send_multicast(void * self, const Message * msg) {
    t_process *self_p = (t_process*) self;
    for (int8_t i = 0; i < self_p->procsNum; ++i)
        if (send(self, i, msg) != 0) return -1;
    return 0;
}

//------------------------------------------------------------------------------

/** Receive a message from the process specified by id.
 *
 * Might block depending on IPC settings.
 *
 * @param self    Any data structure implemented by students to perform I/O
 * @param from    ID of the process to receive message from
 * @param msg     Message structure allocated by the caller
 *
 * @return 0 on success, any non-zero value on error
 */
int
receive(void * self, local_id from, Message * msg) {
    t_process *self_p = (t_process*) self;

    assert(msg != NULL && "Check if receiving message is not null");
    assert(from >= 0 && from <= 10 && "Check whether destination number is valid");
    assert(self_p->procsNum >= 0 && self_p->procsNum <= 10 && "Check whether processes number is valid");
    assert(self_p->sid >= 0 && self_p->sid <= 10 && "Check whether self id is valid");

    MessageHeader mh;
    ssize_t read_result = read(self_p->pipes[from][INPUT], &mh, sizeof(MessageHeader));
    if (read_result <= 0) {
        perror("receive error");
        return -1;
    }

    char payload[MAX_PAYLOAD_LEN];
    if ((int) read(self_p->pipes[from][INPUT], payload, mh.s_payload_len) <= 0) {
        perror("Error on read");
        return -1;
    }
    payload[mh.s_payload_len] = 0;
    msg->s_header = mh;
    strcpy(msg->s_payload, payload);
    return 0;
}

//------------------------------------------------------------------------------

/** Receive a message from any process.
 *
 * Receive a message from any process, in case of blocking I/O should be used
 * with extra care to avoid deadlocks.
 *
 * @param self    Any data structure implemented by students to perform I/O
 * @param msg     Message structure allocated by the caller
 *
 * @return 0 on success, any non-zero value on error
 */
int
receive_any(void * self, Message * msg) {
    printf("Not implemented");
    return 1;
}
